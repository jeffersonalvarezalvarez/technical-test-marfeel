class CircleGraphs extends HTMLElement {
    constructor() {
        super();
        // Initilizing graphs
        this.graphs = [{
            summary : "",
            unit    : "",
            labels  : "",
            values  : "",
            units   : ""
        },
        {
            summary : "",
            unit    : "",
            labels  : "",
            values  : "",
            units   : ""
        },
        {
            summary : "",
            unit    : "",
            labels  : "",
            values  : "",
            units   : ""
        }];
    };

    connectedCallback() {
        // Giving values to graphs
        this.graphs = [{
            summary : this.getAttribute("summary-1") || this.graphs[0].summary,
            unit    : this.getAttribute("unit-1") || this.graphs[0].unit,
            labels  : this.getAttribute("labels-1")
                      .split("|") || this.graphs[0].labels,
            values  : this.getAttribute("values-1")
                      .split("|") || this.graphs[0].values,
            units   : this.getAttribute("units-1")
                      .split("|") || this.graphs[0].units
        },
        {
            summary : this.getAttribute("summary-2") || this.graphs[1].summary,
            unit    : this.getAttribute("unit-2") || this.graphs[1].unit,
            labels  : this.getAttribute("labels-2")
                      .split("|") || this.graphs[1].labels,
            values  : this.getAttribute("values-2")
                      .split("|") || this.graphs[1].values,
            units   : this.getAttribute("units-2")
                      .split("|") || this.graphs[1].units
        },
        {
            summary : this.getAttribute("summary-3") || this.graphs[2].summary,
            unit    : this.getAttribute("unit-3") || this.graphs[2].unit,
            labels  : this.getAttribute("labels-3")
                      .split("|") || this.graphs[2].labels,
            values  : this.getAttribute("values-3")
                      .split("|") || this.graphs[2].values,
            units   : this.getAttribute("units-3")
                      .split("|") || this.graphs[2].units
        }];

        // Showing component
        if(!this.shadowRoot) {
            const sheet = this.getStylesSheets();
            const shadowRoot = this.attachShadow({ mode: "open" });

            shadowRoot.adoptedStyleSheets = [ sheet ];

            const content = this.getCircleCircleGraphs();
            shadowRoot.appendChild(content);
        }
    };

    setGraphs(graphs) {
        this.graphs = graphs;
    };

    getGraphs() {
        return this.graphs;
    };

    getStylesSheets() {
        const sheet = new CSSStyleSheet;

        sheet.replaceSync(
            `.container {
                display: grid;
                grid-template-columns: auto auto auto;
                padding: 10px;
            }
            .col {
                padding: 20px;
                text-align: center;
            }`
        );

        return sheet;
    };

    getCircleGraphsContainer(graph) {
        const graphContainer = document.createElement("div");
        graphContainer.className = "col";

        setGraphics(graph, graphContainer);
        return graphContainer;
    };

    getCircleCircleGraphs() {
        const container = document.createElement("div");
        container.className = "container";

        const graphs = this.graphs;
        for(let i = 0; i < graphs.length; i++) {
            container.appendChild(this.getCircleGraphsContainer(graphs[i]));
        }

        return container;
    };
};

customElements.define("graph-circles", CircleGraphs);

const node = new CircleGraphs;
