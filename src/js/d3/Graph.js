const getRandomArbitrary = (min, max) => {
    return Math.floor(Math.random() * (max - min) + min);
};

const getDataset = (graph) => {
    let dataset = [];

    for(let i = 0; i < graph.labels.length; i++) {
        let percent = 1*(1*graph.values[i]/graph.values.reduce((a, b) => 1*a + 1*b, 0)*100).toFixed(2);
        dataset.push({
            name    : graph.labels[i] + "|" + graph.values[i] + "|" + percent,
            percent : percent
        });
    }

    return dataset
};

const setGraphics = (graph, element) => {    
    let dataset = getDataset(graph);

    // Pie
    let n = dataset.length;
    let w = 300, h = 300;
    let outerRadius = (w/2)-70;
    let innerRadius = 70;

    let colorSchemas = ["schemeAccent", "schemeCategory10", "schemeSet1"];
    let color = d3.scaleOrdinal(d3[colorSchemas[getRandomArbitrary(0, 3)]]);

    let pie = d3.pie().value(d => d.percent).sort(null).padAngle(0);
    let arc = d3.arc().outerRadius(outerRadius).innerRadius(innerRadius);
   
    let svg = d3.select(element)
        .append("svg")
        .attr("width", w)
        .attr("height", h)
        .attr("class", "shadow")
        .append("g")
        .attr("transform", "translate("+w/2+","+h/2+")");

    let path = svg.selectAll("path").data(pie(dataset)).enter().append("path")
        .attr("d", arc)
        .attr("fill", (d, i) => color(d.data.name));

    path.transition().duration(1000).attrTween("d", function(d) {
        let interpolate = d3.interpolate({startAngle: 0, endAngle: 0}, d);
        return (t) => arc(interpolate(t))
    });

    // Description and total
    let description = svg.append("text")
        .attr("y", 0)
        .attr("x", 0)
        .text(graph.summary.toUpperCase());

    description.attr("text-anchor", "middle")
        .attr("font-family", "'Montserrat', sans-serif")
        .style("fill", "rgb(121 ,142, 156)")
        .style("font-weight", "800")
        .attr("font-size", "15px");

    let total = svg.append("text")
        .attr("y", 20)
        .attr("x", 0)
        .text(graph.unit + " " + graph.values.reduce((a, b) => 1*a + 1*b, 0))
        
    total.attr("text-anchor", "middle")
        .attr("font-family", "'Montserrat', sans-serif")
        .style("font-weight", "800")
        .attr("font-size", "20px");

    // Legend
    var itemWidth = 150;
    let legend = svg.selectAll(".legend")
        .data(color.domain())
        .enter()
        .append("g")
        .attr("transform", (d,i) => "translate(" + (i%n * itemWidth - 150) + ", 100)")
        .attr("class", "legend")
 
    legend.append("text")
        .attr("x", 30)
        .attr("y", 15)
        .text((d) => d.split("|")[0])
        .style("fill", color)
        .style("font-size", "14px")
        .style("font-weight", "800")
        .attr("font-family", "'Montserrat', sans-serif");

    legend.append("text")
        .attr("x", 30)
        .attr("y", 30)
        .text((d) => "%" + d.split("|")[2])
        .style("font-size", "14px")
        .attr("font-family", "'Montserrat', sans-serif");

    legend.append("text")
        .attr("x", 90)
        .attr("y", 30)
        .text((d) => graph.unit + d.split("|")[1])
        .style("fill", "rgb(121 ,142, 156)")
        .style("font-size", "14px")
        .attr("font-family", "'Montserrat', sans-serif");
};
