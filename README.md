# Welcome to my technical test

Hi! I'm Jefferson Alvarez and I did this little repository to show my technical test for  **TEST JAVASCRIPT / FRONTEND DEVELOPER**.


# Application

## Start page

If you want to see my advance, you only have to open index file of the project. It is in src/index.html. I used Vanilla JavaScript ECS6 and D3.js.

## Tests

If you want to test the application, you have to open test file. It is in test/test.html. I used Jasmine for this purpose.

## Thanks and I am looking for your feedback!
