describe("Testing the functionality of graphs", () => {
    it("Should render and showing correct values in graphics", () => {
        const graphComponent = new CircleGraphs();
        const graphs = [{
            summary : "Victorias en fútbol",
            unit    : "victorias",
            labels  : "Argentina|Colombia",
            values  : "7|5",
            units   : "partidos|partidos"
        },
        {
            summary : "Mejor economía",
            unit    : "$",
            labels  : "Argentina|Colombia",
            values  : "7000|8000",
            units   : "pesos|pesos"
        },
        {
            summary : "Mejor avance",
            unit    : "%",
            labels  : "Argentina|Colombia",
            values  : "70|30",
            units   : "avance|avance"
        }];
        graphComponent.setGraphs(graphs)
        expect(graphComponent.getGraphs()).toBe(graphs);
    });
});
